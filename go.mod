module main

go 1.15

require (
	github.com/zmb3/spotify v1.1.2
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
