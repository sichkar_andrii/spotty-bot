package main

import (
	"fmt"
	"log"
	"net/http"
	// "io"
	"main/profile"
	"main/playlist"
	// "strings"
	// "net"

	"github.com/zmb3/spotify"
)

// redirectURI is the OAuth redirect URI for the application.
// You must register an application at Spotify's developer portal
// and enter this value.
const redirectURI = "http://localhost:8082/callback"

var (
	auth  = spotify.NewAuthenticator(
		redirectURI, 
		spotify.ScopeUserReadCurrentlyPlaying, 
		spotify.ScopeUserReadPlaybackState, 
		spotify.ScopeUserModifyPlaybackState,
		spotify.ScopeUserReadRecentlyPlayed,
	)
	ch    = make(chan *spotify.Client)
	state = "abc123"
	userID = "31gz6rgfvggtm277aonrao2mg3ae"
)

func main() {
	// var client *spotify.Client
	auth.SetAuthInfo("aef73fac407c4428a8881a4e18e028ca", "5b8bee8f6c1545f89464aad509fc2f7b")
	// var playerState *spotify.PlayerState
	// var client *spotify.Client

	// go http.ListenAndServe(":8081", nil)

	// first start an HTTP server
	// http.HandleFunc("/callback", completeAuth)
	http.HandleFunc("/callback", func(w http.ResponseWriter, r *http.Request) {
		
		spotifyClient := completeAuth(w,r)
		fmt.Println("You are logged in")
			// use the client to make calls that require authorization
			user, err := spotifyClient.CurrentUser()
			if err != nil {
				log.Fatal(err)
			}
			CallUser(profile.Profile{}, user.ID)				
			// fmt.Print(spotifyClient.PlayerRecentlyPlayed())
	})

	// kfdab1n0d5qtohad0dgv0dcwm
	http.HandleFunc("/gg", func(w http.ResponseWriter, r *http.Request) {
		// fmt.Println("You are logged in")
		// CallUser(profile.Profile{}, userID)				
	})
	
	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		// c := spotify.ScopeUserReadRecentlyPlayed
		url := auth.AuthURL(state)
		
		
		// fmt.Fprintf(w, url)
		fmt.Println("Please log in to Spotify by visiting the following page in your browser:", url)

		http.Redirect(w, r, url, http.StatusSeeOther)
	})
	
	http.ListenAndServe(":8082", nil)
	// wait for auth to complete
	// clientChan := <-ch

	// use the client to make calls that require authorization
	// user, err := clientChan.CurrentUser()
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// createEmployeeHanlder := http.HandlerFunc(createEmployee)

	// http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	fmt.Println("You are logged in as:", user.ID)
	// 	// CallUser(profile.Profile{}, user.ID)
	// })
	// CallUser(profile.Profile{}, user.ID)

	// fmt.Println("You are logged in as:", user.ID)
}

func CallUser(p profile.Profile, userId string){
	fmt.Println("We`ve called user")
	fmt.Println(userId)

	p.SetUserId(userId)
	p.Get()
}

func completeAuth(w http.ResponseWriter, r *http.Request) spotify.Client {
	tok, err := auth.Token(state, r)
	fmt.Println(w, tok.AccessToken)
	// fmt.Println("ssss")
	// fmt.Fprintf(w, tok.RefreshToken)
	if err != nil {
		http.Error(w, "Couldn't get token", http.StatusForbidden)
		log.Fatal(err)
	}
	if st := r.FormValue("state"); st != state {
		http.NotFound(w, r)
		log.Fatalf("State mismatch: %s != %s\n", st, state)
	}
	// use the token to get an authenticated client
	client := auth.NewClient(tok)
	return client
}